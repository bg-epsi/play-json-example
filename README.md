# play-json-example

A simple example showing how to use Play JSON to marshall and unmarshall a case class.

To execute: `sbt run`