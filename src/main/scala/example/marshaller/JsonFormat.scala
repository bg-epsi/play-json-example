package example.marshaller

import org.joda.time.DateTime
import play.api.libs.json._

/** Holds JSON formatters - order matters, children formats must be declared before parents */
object JsonFormat {

  import example.domain.Model._
  import example.domain.Model.State._

  // custom read/write formats to handle the State enumeration (libraries exist to do this but added it here for example)
  implicit val stateFormat: Format[State] = new Format[State] {
    def reads(json: JsValue) = JsSuccess(State.withName(json.as[String]))
    def writes(state: State.Value) = JsString(state.toString)
  }

  // read/write formats for Joda DateTime (this is older way, can just import this definition now with Play 2.6.x)
  val pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'"
  implicit val dateFormat: Format[DateTime] = Format[DateTime](Reads.jodaDateReads(pattern), Writes.jodaDateWrites(pattern))

  // declare JSON formats for our case classes
  implicit val addressFormat: Format[Address] = Json.format[Address]
  implicit val hispitalFormat: Format[Hospital] = Json.format[Hospital]

}
