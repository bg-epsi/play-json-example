package example

import example.domain.Model._
import example.marshaller.JsonFormat._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.Json

object Main extends App {

  val hospital = Hospital("Acme Hospital",
                  Address("123 Anywhere St", "Somewhere", State.MO),
                   DateTime.now(DateTimeZone.UTC))

  val json = Json.prettyPrint(Json.toJson(hospital))

  println(json)

  val obj = Json.fromJson[Hospital](Json.parse(json)).asOpt

  println(obj)

}
