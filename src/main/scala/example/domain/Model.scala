package example.domain

import org.joda.time.DateTime

object Model {

  object State extends Enumeration {
    type State = Value
    val AK, MO, IL, TX = Value
  }

  case class Address(street: String, city: String, state: State.Value)

  case class Hospital(name: String, address: Address, modified: DateTime)

}