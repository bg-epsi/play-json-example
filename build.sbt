
name := "play-json-example"

scalaVersion := "2.12.4"

version := "1.0-SNAPSHOT"

// https://mvnrepository.com/artifact/joda-time/joda-time
libraryDependencies += "joda-time" % "joda-time" % "2.9.9"

// https://mvnrepository.com/artifact/com.typesafe.play/play-json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.8"

mainClass in (Compile,run) := Some("example.Main")
